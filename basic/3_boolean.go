//tipe data boolean kata kuncinya : bool dan hanya memiliki nilai true atau false

package main

import "fmt"

func main() {
	fmt.Println("Benar = ", true)
	fmt.Println("Salah = ", false)
}
