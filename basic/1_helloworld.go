//selalu diawali dengan menuliskan package main
package main

import "fmt"

//package untuk menampilkan tulisan

//program di code di dalam function main
func main() {
	fmt.Println("Hello World")
}

//untuk menjalankan go gunakan perintah :
// 1. mengcompile : go build <nama_file + extensinya misal helloworld.go>
//		cara ini akan membuat file binary dengan extensi .exe kalo di windows dan dijalankan melalui terminal
// 2. cara dua langsung di run tanpa perlu di compile : go run <nama_file + extensinya misal helloworld.go>
