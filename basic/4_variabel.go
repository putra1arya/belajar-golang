//untuk membuat variable dalam go dilakukan dengan cara : var <nama_variable> <tipe_data>
// 1 variable hanya bisa dipakai untuk tipe data yang sama dan tidak dapat di deklarasikan ulang di dalam 1 program
package main

import "fmt"

func main() {
	//jika kita hanya membuat variable tanpa langsung memasukan datanya maka variable wajib diberikan tipe data
	var name string

	name = "Arya Putra"
	fmt.Println(name)
	name = "Arya Tirtana"
	fmt.Println(name)

	//jika data langsung diberikan makan tidak perlu diberikan tipe data
	var angka = 34
	fmt.Println(angka)

	//kata var tidak wajib digunakan saat membuat variable kata "var" bisa diganti menggunakan :=

	sekolah := "Smansa"
	fmt.Println(sekolah)
	sekolah = "Smanela"
	fmt.Println(sekolah)

	//membuat multiple variable
	var (
		firstname = "Budi"
		lastname  = "Setiawan"
	)
	fmt.Println("Hello ", firstname, lastname)
	var (
		age     uint8
		address string
	)
	age = 23
	address = "Indonesia"
	fmt.Println("Your Age : ", age)
	fmt.Println("Your Address : ", address)

	//CONSTRANT
	//constrant adalah variable yang nilainya tidak bisa diubah lagi setelah pertama kali diberi nilai
	//kata kuncinya : const
	//wajib langsung mengisi constantnya

	const phi = 3.14
	fmt.Println(phi)
	//output = 3.14

	//multiple constrant
	const (
		satu string = "Budi"
		dua         = "Setiawan"
	)
	fmt.Println("Hello ", satu, dua)
	//output = Hello Budi Setiawan

}
