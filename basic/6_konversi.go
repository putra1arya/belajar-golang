//Mengkonversi variable
package main

import (
	"fmt"
)

func main() {
	var angka1 int32 = 129
	fmt.Println(angka1) // output 129
	var angka2 int64 = int64(angka1)
	fmt.Println(angka2) // output 129

	var angka3 int8 = int8(angka2)
	fmt.Println(angka3)
	//output -127 (karena nilai max dari int8 adalah 128 jika melebihi itu maka akan kembali ke yang terkecil yaitu -127)

	var name = "Arya"
	var a = name[0]
	fmt.Println(a) //outputnya A dengan tipe data byte/uint8
	var aString = string(a)
	fmt.Println(aString) //outputnya A tipe data string

}
