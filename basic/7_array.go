//tipe data array
// untuk membuat variable dengan tipe data array maka harus ditentukan dulu maksimum kapasitasnya dan kapasitas tersebut tidak dapat ditambah lagi
// var/const <nama_array> [<kapasitas>] <type data>

package main

import "fmt"

func main() {
	//cara manual
	var name [3]string
	name[0] = "Arya"
	name[1] = "Tirta"
	name[2] = "Putra"
	fmt.Println(name) //output [Arya Tirta Putra]

	//cara langsung isi data
	// var/const <nama_array> = [<kapasitas>] <type_data> { <valuenya> }
	//value diakhiri dengan tanda koma ',' jika tidak error
	var angka = [3]int{
		1,
		2,
		3,
	}
	fmt.Println(angka) //outputnya [1 2 3]

	//function pada array

	//1. len => untuk mendapatkan panjang array
	fmt.Println(len(name)) // outputnya 3

}
