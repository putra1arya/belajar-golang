package main

import "fmt"

func main() {
	obj := map[string]string{
		"name": "arya",
		"age":  "24",
	}
	fmt.Println(obj)
	fmt.Println(obj["name"])

	//nambah data baru
	obj["kerjaan"] = "Programmer"
	fmt.Println(obj)

	//FUNCTION PADA MAP
	//1. len(map) => untuk ngambil panjang map
	fmt.Println(len(obj)) //outputnya 3

	//2. make(map[tipekey]tipevalue) => untuk membuat map baru tanpa isi
	obj2 := make(map[string]int)
	obj2["Nomor Undian"] = 2
	fmt.Println(obj2)

	//3. delete(map,key) => untuk menghapus data pada map menggunakan key
	delete(obj, "kerjaan")
	fmt.Println(obj)

}
