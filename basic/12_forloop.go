package main

import "fmt"

func main() {
	friends := []string{"budi", "anton", "yudi"}

	for i := 0; i < len(friends); i++ {
		fmt.Println(friends[i])
	}
	//atau
	count := 0
	for count < len(friends) {
		fmt.Println("Masih Belum Count : ", count)
		count++
	}

	//for range
	for index, friend := range friends {
		fmt.Println("Index ke : ", index, "Siapa ", friend)
	}
}
