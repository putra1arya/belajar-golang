//tipe data slice mirip seperti array yang membedakan adalah ukurannya bisa berubah
//tipe data slice memiliki 3 data yaitu pointer, length, dan capacity
// 1. Pointer adalah penunjuk pertama di array pada slice
// 2. Length adalah panjang dari slice
// 3. Capacity adalah kapasitas dari slice, dimana length tidak boleh lebih besar dari capacity

package main

import "fmt"

func main() {
	var bulan = [12]string{
		"Januari",
		"Febuari",
		"Maret",
		"April",
		"May",
		"Juni",
		"Juli",
		"Agustus",
		"September",
		"Oktober",
		"November",
		"Desember",
	}
	fmt.Println(bulan)

	//cara membuat slice
	//1. array[low:high] => Membuat slice dari index ke low hingga index sebelum high
	var bulan1 = bulan[3:7]

	fmt.Println(bulan1)      // outputnya [april may juni juli] karena april index ke 3 dan juli index ke 6
	fmt.Println(len(bulan1)) //outputnya 4 karena jumlah datanya 4
	fmt.Println(cap(bulan1)) // outputnya 9 karena diambil dari index ke 3 => 12 - 3 = 9

	//2. array[low:] => Membuat slice dari index ke low hingga index terakhir di array
	var bulan2 = bulan[9:]
	fmt.Println(bulan2) // outputnya [oktober november desember] karena oktober index ke 9

	//3. array[:high] => Membuat slice dari index ke 0 hingga index sebelum high
	var bulan3 = bulan[:6]
	fmt.Println(bulan3) //outputnya [Januari febuari maret april may juni]

	//4. array[:] => Membuat slice dari index ke 0 hingga index terakhir di array
	var bulan4 = bulan[:]
	fmt.Println(bulan4) //outputnya sama dengan array bulan

	// nb: kalo array/slice diubah maka yang lainnya juga ikut keubah
	bulan[0] = "minggu"
	fmt.Println(bulan)  //outputnya [minggu Febuari Maret April May Juni Juli Agustus September Oktober November Desember]
	fmt.Println(bulan3) // outputnya [minggu Febuari Maret April May Juni]

	// begitu pula sebaliknya
	bulan2[2] = "Sebelumnya Desember"
	fmt.Println(bulan2) //outputnya [Oktober November Sebelumnya Desember]
	fmt.Println(bulan)  // outputnya [minggu Febuari Maret April May Juni Juli Agustus September Oktober November Sebelumnya Desember]

	//FUNCTION PADA SLICE
	//1. len => untuk mendapatkan jumlah data pada slice
	fmt.Println(len(bulan1)) //outputnya 4 karena jumlah datanya 4

	//2. cap => untuk mendapatkan kapasitas slice
	fmt.Println(cap(bulan1)) // outputnya 9 karena diambil dari index ke 3 => 12 - 3 = 9

	//3. append(slice,data) => untuk membuat slice baru dengan menambahkan data ke posisi terakhir pada slice, jika kapasitas penuh maka dibuat array baru
	days := [...]string{"senin", "selasa", "rabu", "kamis", "jumat", "sabtu", "minggu"}
	daysSlice1 := days[:3]
	daysSlice1 = append(daysSlice1, "Libur")
	fmt.Println(daysSlice1)

	//4. make([]<type_data>,<length>,<capacity>) => untuk membuat slice baru tanpa mengacu pada sebuah array
	newSlice := make([]string, 2, 10)
	newSlice = append(newSlice, "Pertama")
	newSlice = append(newSlice, "Kedua")
	fmt.Println(newSlice)

	//5. copy(<slice_dituju>,<slice_dicopy>) => untuk mengcopy isi dari slice
	copySlice := make([]string, len(daysSlice1), cap(daysSlice1))
	copy(copySlice, daysSlice1)
	fmt.Println(copySlice)

	//hati-hati ketika membuat array atau slice
	iniArray := [...]int{1, 2, 3, 4, 5}
	iniSlice := []int{1, 2, 3, 4, 5}
	fmt.Println(iniArray)
	fmt.Println(iniSlice)
}
