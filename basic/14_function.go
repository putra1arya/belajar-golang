package main

import "fmt"

func perhitungan(a int, b int) int {
	hasil := a + b
	return hasil
}

//untuk mereturn value lebih dari satu tipe data return juga harus sesuai dengan jumlah data yang ingin direturn walaupun tipe datanya sama
func multiValue() (string, int) {
	return "Ini Angka", 2
}

//memberi nama pada value return
func namaReturn() (nama, negara string, umur int8) {
	nama = "Arya"
	negara = "Indonesia"
	umur = 24

	return //karena sudah dikasi nama tidak perlu lagi dituliskan
}

//Variadic function => function yang memiliki vararg sebagai parameternya vararg itu seperti splice tapi bisa banyak
//variabel argumen (vararg) hanya bisa ditaro di paling belakang misal func random(nama string, numbers ...int)
func variadicSum(numbers ...int) int {
	jumlah := 0
	for _, number := range numbers {
		jumlah += number
	}
	return jumlah
}

func main() {
	varA := 5
	varB := 8
	fmt.Println("Hasilnya Adalah ", perhitungan(varA, varB))
	//untuk menangkap data dari function yang mereturn multiple value bisa menggunakan cara dibawah
	kata, angka := multiValue()
	fmt.Println("Result : ", kata, angka)
	fmt.Println(multiValue()) //bisa diprint langsung outpunya jadi "Ini Angka 2"

	//untuk meng ignore salah satu value dari multiple value function bisa menggunakan _
	_, numb := multiValue()
	fmt.Println(numb * 2) //outputnya 4

	//nama value return
	nam, _, umur := namaReturn()
	fmt.Println(nam, umur)

	total := variadicSum(5, 3, 21, 35, 5)
	fmt.Println(total) //outputnya 69

	//slice sebagai parameter untuk varg
	slice := []int{1, 2, 3, 4, 5}
	fmt.Println(variadicSum(slice...))
}
