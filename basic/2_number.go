//tipe data integer dalam go ada beberapa yaitu
// 1. int dibagi menjadi 4 yaitu :
//  	a. int8 => nilai minimum -127 dan nilai maxnya 128
//		b. int16 => nilai minimum -32767 dan nilai maxnya 32768
//		c. int32 => nilai minimum -2147483647 dan nilai maxnya 2147483648
//		d. int64 => nilai minimum -9223372036854775807 dan nilai maxnya 9223372036854775808

// 2. uint dibagi menjadi 4 juga yaitu :
//  	a. uint8 => nilai minimum 0 dan nilai maxnya 255
//		b. uint16 => nilai minimum 0 dan nilai maxnya 65535
//		c. uint32 => nilai minimum 0 dan nilai maxnya 4294967295
//		d. uint64 => nilai minimum 0 dan nilai maxnya 18446744073709551615

// untuk tipe data float ada float32 dan float 64.
// float32 min 1.8 x pangkat -38
// float64 min 1.8 x pangkat -308

//alias
// byte untuk uint8
// rune untuk int32
// int minimal int32
// uint minimal uint32

package main

import "fmt"

func main() {
	fmt.Println("Satu = ", 1)
	fmt.Println("Dua = ", 2)
	fmt.Println("Tiga koma lima = ", 3.5)

}
