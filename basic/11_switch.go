package main

import "fmt"

func main() {

	month := "Maret"
	switch month {
	case "April":
		fmt.Println("Bulan April")
	case "Maret":
		fmt.Println("Bulan Maret")
	default:
		fmt.Println("12 Bulan")
	}
	//short statement switch
	switch bulan := "Maret"; bulan {
	case "April":
		fmt.Println("Bulan April")
	case "Mei":
		fmt.Println("Bulan Mei")
	default:
		fmt.Println("12 Bulan")
	}

	//switch bisa tanpa menggunakan expression
	panjang := len(month)
	switch {
	case panjang > 5:
		fmt.Println("Kepanjangan")
	default:
		fmt.Println("Ini Baru Pas")
	}

}
