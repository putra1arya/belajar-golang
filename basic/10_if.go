package main

import "fmt"

func main() {
	name := "Eko"

	if name == "Eko" {
		fmt.Println("Benar")
	} else {
		fmt.Println("Salah")
	}

	// short statement dalam if => untuk menklarasikan sesuatu sebelum kondisinya

	if age := "Bios"; age != name {
		fmt.Println("salah")
	}
}
