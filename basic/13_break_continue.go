//break dan continue adalah kata kunci yang dapat digunakan untuk menghentikan perulangan
// break => menghentikan semua perulangan
// continue => menghentikan perulangan yang sedang berjalan, lalu melanjutkan perulangan selanjutnya

package main

import "fmt"

func main() {
	for i := 0; i < 10; i++ {
		if i == 5 {
			break
		}
		fmt.Println("Perulangan ke - ", i)
	}
	//outputnya hanya sampai "perulangan ke 4" karena break menghentikan semua perulangan serta proces di bawahnya( masih di dalem loop)

	fmt.Println("\n")
	fmt.Println("Perulangan ke 2")
	for i := 0; i < 10; i++ {
		if i == 5 {
			continue
		}
		fmt.Println("Perulangan ke - ", i)
	}
	//outputnya akan sampai "perulangan ke 9" akan tetapi "perulangan ke 5" tidak ada
}
